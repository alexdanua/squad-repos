package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

@ProcessedRootSentinel(
    roots = "com.danich.gitlist.RepoListFragmentTest"
)
public final class _com_danich_gitlist_RepoListFragmentTest {
}
