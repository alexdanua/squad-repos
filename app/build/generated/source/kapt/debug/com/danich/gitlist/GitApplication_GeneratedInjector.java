package com.danich.gitlist;

import dagger.hilt.InstallIn;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = GitApplication.class
)
@GeneratedEntryPoint
@InstallIn(SingletonComponent.class)
public interface GitApplication_GeneratedInjector {
  void injectGitApplication(GitApplication gitApplication);
}
