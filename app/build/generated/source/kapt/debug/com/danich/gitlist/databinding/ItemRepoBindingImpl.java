package com.danich.gitlist.databinding;
import com.danich.gitlist.R;
import com.danich.gitlist.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemRepoBindingImpl extends ItemRepoBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.appcompat.widget.LinearLayoutCompat mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemRepoBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private ItemRepoBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            );
        this.mboundView0 = (androidx.appcompat.widget.LinearLayoutCompat) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.repoEntity == variableId) {
            setRepoEntity((com.danich.gitlist.domain.models.Repo) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setRepoEntity(@Nullable com.danich.gitlist.domain.models.Repo RepoEntity) {
        this.mRepoEntity = RepoEntity;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.repoEntity);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String repoEntityName = null;
        java.lang.String repoEntityDescription = null;
        com.danich.gitlist.domain.models.Repo repoEntity = mRepoEntity;
        java.lang.String javaLangStringDescriptionRepoEntityDescription = null;
        java.lang.String javaLangStringNameRepoEntityName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (repoEntity != null) {
                    // read repoEntity.name
                    repoEntityName = repoEntity.getName();
                    // read repoEntity.description
                    repoEntityDescription = repoEntity.getDescription();
                }


                // read ("Name: ") + (repoEntity.name)
                javaLangStringNameRepoEntityName = ("Name: ") + (repoEntityName);
                // read ("Description: ") + (repoEntity.description)
                javaLangStringDescriptionRepoEntityDescription = ("Description: ") + (repoEntityDescription);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, javaLangStringNameRepoEntityName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, javaLangStringDescriptionRepoEntityDescription);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): repoEntity
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}