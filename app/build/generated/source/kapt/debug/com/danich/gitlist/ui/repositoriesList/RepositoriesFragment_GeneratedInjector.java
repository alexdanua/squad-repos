package com.danich.gitlist.ui.repositoriesList;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = RepositoriesFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface RepositoriesFragment_GeneratedInjector {
  void injectRepositoriesFragment(RepositoriesFragment repositoriesFragment);
}
