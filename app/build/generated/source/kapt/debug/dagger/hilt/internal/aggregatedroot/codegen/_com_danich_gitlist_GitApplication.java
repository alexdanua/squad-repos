package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.danich.gitlist.GitApplication",
    originatingRoot = "com.danich.gitlist.GitApplication",
    rootAnnotation = HiltAndroidApp.class
)
public class _com_danich_gitlist_GitApplication {
}
