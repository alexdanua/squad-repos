package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.danich.gitlist.ui.repositoriesList.RepositoriesViewModel_HiltModules.BindsModule"
)
public class _com_danich_gitlist_ui_repositoriesList_RepositoriesViewModel_HiltModules_BindsModule {
}
