package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityComponent",
    entryPoints = "com.danich.gitlist.ui.startActivity.MainActivity_GeneratedInjector"
)
public class _com_danich_gitlist_ui_startActivity_MainActivity_GeneratedInjector {
}
