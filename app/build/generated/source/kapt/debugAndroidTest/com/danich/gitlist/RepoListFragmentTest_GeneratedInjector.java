package com.danich.gitlist;

import dagger.hilt.InstallIn;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = RepoListFragmentTest.class
)
@GeneratedEntryPoint
@InstallIn(SingletonComponent.class)
public interface RepoListFragmentTest_GeneratedInjector {
  void injectTest(RepoListFragmentTest repoListFragmentTest);
}
