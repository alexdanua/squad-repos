package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.testing.HiltAndroidTest;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.danich.gitlist.RepoListFragmentTest",
    originatingRoot = "com.danich.gitlist.RepoListFragmentTest",
    rootAnnotation = HiltAndroidTest.class
)
public class _com_danich_gitlist_RepoListFragmentTest {
}
