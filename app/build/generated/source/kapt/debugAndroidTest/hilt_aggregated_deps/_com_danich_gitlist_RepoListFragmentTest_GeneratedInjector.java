package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    test = "com.danich.gitlist.RepoListFragmentTest",
    entryPoints = "com.danich.gitlist.RepoListFragmentTest_GeneratedInjector"
)
public class _com_danich_gitlist_RepoListFragmentTest_GeneratedInjector {
}
