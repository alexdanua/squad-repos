package com.danich.gitlist.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\f"}, d2 = {"Lcom/danich/gitlist/data/repository/RepoRepositoryImpl;", "Lcom/danich/gitlist/domain/interfaces/RepoRepository;", "apiService", "Lcom/danich/gitlist/network/ApiService;", "repoMapper", "Lcom/danich/gitlist/network/mappers/RepoMapper;", "(Lcom/danich/gitlist/network/ApiService;Lcom/danich/gitlist/network/mappers/RepoMapper;)V", "getRepos", "Lcom/danich/gitlist/data/ResultData;", "", "Lcom/danich/gitlist/domain/models/Repo;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class RepoRepositoryImpl implements com.danich.gitlist.domain.interfaces.RepoRepository {
    private final com.danich.gitlist.network.ApiService apiService = null;
    private final com.danich.gitlist.network.mappers.RepoMapper repoMapper = null;
    
    @javax.inject.Inject()
    public RepoRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.danich.gitlist.network.ApiService apiService, @org.jetbrains.annotations.NotNull()
    com.danich.gitlist.network.mappers.RepoMapper repoMapper) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getRepos(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.danich.gitlist.data.ResultData<? extends java.util.List<com.danich.gitlist.domain.models.Repo>>> continuation) {
        return null;
    }
}