package com.danich.gitlist.domain.usecases;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006H\u0086B\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\n"}, d2 = {"Lcom/danich/gitlist/domain/usecases/GetReposUseCase;", "", "repository", "Lcom/danich/gitlist/domain/interfaces/RepoRepository;", "(Lcom/danich/gitlist/domain/interfaces/RepoRepository;)V", "invoke", "Lcom/danich/gitlist/data/ResultData;", "", "Lcom/danich/gitlist/domain/models/Repo;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class GetReposUseCase {
    private final com.danich.gitlist.domain.interfaces.RepoRepository repository = null;
    
    @javax.inject.Inject()
    public GetReposUseCase(@org.jetbrains.annotations.NotNull()
    com.danich.gitlist.domain.interfaces.RepoRepository repository) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object invoke(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.danich.gitlist.data.ResultData<? extends java.util.List<com.danich.gitlist.domain.models.Repo>>> continuation) {
        return null;
    }
}