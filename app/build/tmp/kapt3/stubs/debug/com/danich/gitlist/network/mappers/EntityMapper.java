package com.danich.gitlist.network.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\bf\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003J\u0015\u0010\u0004\u001a\u00028\u00012\u0006\u0010\u0005\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/danich/gitlist/network/mappers/EntityMapper;", "RemoteModel", "DomainModel", "", "mapFromRemote", "remoteModel", "(Ljava/lang/Object;)Ljava/lang/Object;", "app_debug"})
public abstract interface EntityMapper<RemoteModel extends java.lang.Object, DomainModel extends java.lang.Object> {
    
    public abstract DomainModel mapFromRemote(RemoteModel remoteModel);
}