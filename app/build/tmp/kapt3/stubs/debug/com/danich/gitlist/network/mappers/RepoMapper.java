package com.danich.gitlist.network.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016J\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00020\b\u00a8\u0006\n"}, d2 = {"Lcom/danich/gitlist/network/mappers/RepoMapper;", "Lcom/danich/gitlist/network/mappers/EntityMapper;", "Lcom/danich/gitlist/network/models/RepoRemote;", "Lcom/danich/gitlist/domain/models/Repo;", "()V", "mapFromRemote", "entity", "mapFromRemoteList", "", "remotePosts", "app_debug"})
public final class RepoMapper implements com.danich.gitlist.network.mappers.EntityMapper<com.danich.gitlist.network.models.RepoRemote, com.danich.gitlist.domain.models.Repo> {
    
    @javax.inject.Inject()
    public RepoMapper() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.danich.gitlist.domain.models.Repo> mapFromRemoteList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.danich.gitlist.network.models.RepoRemote> remotePosts) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.danich.gitlist.domain.models.Repo mapFromRemote(@org.jetbrains.annotations.NotNull()
    com.danich.gitlist.network.models.RepoRemote entity) {
        return null;
    }
}