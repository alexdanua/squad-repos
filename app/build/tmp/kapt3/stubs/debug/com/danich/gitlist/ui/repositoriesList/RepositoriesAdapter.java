package com.danich.gitlist.ui.repositoriesList;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0013B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0014\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005J\b\u0010\n\u001a\u00020\u000bH\u0016J\u001c\u0010\f\u001a\u00020\b2\n\u0010\r\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u000bH\u0016J\u001c\u0010\u000f\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000bH\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/danich/gitlist/ui/repositoriesList/RepositoriesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/danich/gitlist/ui/repositoriesList/RepositoriesAdapter$RepoViewHolder;", "()V", "repoList", "", "Lcom/danich/gitlist/domain/models/Repo;", "addItems", "", "items", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "RepoViewHolder", "app_debug"})
public final class RepositoriesAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.danich.gitlist.ui.repositoriesList.RepositoriesAdapter.RepoViewHolder> {
    private java.util.List<com.danich.gitlist.domain.models.Repo> repoList;
    
    public RepositoriesAdapter() {
        super();
    }
    
    public final void addItems(@org.jetbrains.annotations.NotNull()
    java.util.List<com.danich.gitlist.domain.models.Repo> items) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.danich.gitlist.ui.repositoriesList.RepositoriesAdapter.RepoViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.danich.gitlist.ui.repositoriesList.RepositoriesAdapter.RepoViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/danich/gitlist/ui/repositoriesList/RepositoriesAdapter$RepoViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/danich/gitlist/databinding/ItemRepoBinding;", "(Lcom/danich/gitlist/ui/repositoriesList/RepositoriesAdapter;Lcom/danich/gitlist/databinding/ItemRepoBinding;)V", "bind", "", "entity", "Lcom/danich/gitlist/domain/models/Repo;", "app_debug"})
    public final class RepoViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.danich.gitlist.databinding.ItemRepoBinding binding = null;
        
        public RepoViewHolder(@org.jetbrains.annotations.NotNull()
        com.danich.gitlist.databinding.ItemRepoBinding binding) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.Nullable()
        com.danich.gitlist.domain.models.Repo entity) {
        }
    }
}