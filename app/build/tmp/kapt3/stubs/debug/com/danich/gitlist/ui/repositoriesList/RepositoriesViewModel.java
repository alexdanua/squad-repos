package com.danich.gitlist.ui.repositoriesList;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0012\u001a\u00020\u0013R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0010\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u00070\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000f\u00a8\u0006\u0014"}, d2 = {"Lcom/danich/gitlist/ui/repositoriesList/RepositoriesViewModel;", "Landroidx/lifecycle/ViewModel;", "getReposUseCase", "Lcom/danich/gitlist/domain/usecases/GetReposUseCase;", "(Lcom/danich/gitlist/domain/usecases/GetReposUseCase;)V", "_errorText", "Landroidx/lifecycle/MutableLiveData;", "Lcom/danich/gitlist/utils/Event;", "", "_repoList", "", "Lcom/danich/gitlist/domain/models/Repo;", "errorText", "Landroidx/lifecycle/LiveData;", "getErrorText", "()Landroidx/lifecycle/LiveData;", "repoList", "getRepoList", "getRepos", "", "app_debug"})
public final class RepositoriesViewModel extends androidx.lifecycle.ViewModel {
    private final com.danich.gitlist.domain.usecases.GetReposUseCase getReposUseCase = null;
    private final androidx.lifecycle.MutableLiveData<com.danich.gitlist.utils.Event<java.lang.String>> _errorText = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<com.danich.gitlist.utils.Event<java.lang.String>> errorText = null;
    private final androidx.lifecycle.MutableLiveData<com.danich.gitlist.utils.Event<java.util.List<com.danich.gitlist.domain.models.Repo>>> _repoList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<com.danich.gitlist.utils.Event<java.util.List<com.danich.gitlist.domain.models.Repo>>> repoList = null;
    
    @javax.inject.Inject()
    public RepositoriesViewModel(@org.jetbrains.annotations.NotNull()
    com.danich.gitlist.domain.usecases.GetReposUseCase getReposUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.danich.gitlist.utils.Event<java.lang.String>> getErrorText() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.danich.gitlist.utils.Event<java.util.List<com.danich.gitlist.domain.models.Repo>>> getRepoList() {
        return null;
    }
    
    public final void getRepos() {
    }
}