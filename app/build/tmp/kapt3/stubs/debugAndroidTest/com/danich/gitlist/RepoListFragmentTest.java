package com.danich.gitlist;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0007J\b\u0010\u0017\u001a\u00020\u0016H\u0007J\b\u0010\u0018\u001a\u00020\u0016H\u0007J\b\u0010\u0019\u001a\u00020\u0016H\u0007J\b\u0010\u001a\u001a\u00020\u0016H\u0007R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u00020\n8GX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u001b"}, d2 = {"Lcom/danich/gitlist/RepoListFragmentTest;", "", "()V", "getReposUseCase", "Lcom/danich/gitlist/domain/usecases/GetReposUseCase;", "getGetReposUseCase", "()Lcom/danich/gitlist/domain/usecases/GetReposUseCase;", "setGetReposUseCase", "(Lcom/danich/gitlist/domain/usecases/GetReposUseCase;)V", "hiltRule", "Ldagger/hilt/android/testing/HiltAndroidRule;", "getHiltRule", "()Ldagger/hilt/android/testing/HiltAndroidRule;", "setHiltRule", "(Ldagger/hilt/android/testing/HiltAndroidRule;)V", "viewModel", "Lcom/danich/gitlist/ui/repositoriesList/RepositoriesViewModel;", "getViewModel", "()Lcom/danich/gitlist/ui/repositoriesList/RepositoriesViewModel;", "setViewModel", "(Lcom/danich/gitlist/ui/repositoriesList/RepositoriesViewModel;)V", "init", "", "mainActivityTest", "repositoriesFragmentTest", "testBtnRetry", "testRepositoriesViewModel", "app_debug"})
@org.junit.runner.RunWith(value = androidx.test.ext.junit.runners.AndroidJUnit4.class)
@dagger.hilt.android.testing.HiltAndroidTest()
public final class RepoListFragmentTest {
    public com.danich.gitlist.ui.repositoriesList.RepositoriesViewModel viewModel;
    @javax.inject.Inject()
    public com.danich.gitlist.domain.usecases.GetReposUseCase getReposUseCase;
    @org.jetbrains.annotations.NotNull()
    private dagger.hilt.android.testing.HiltAndroidRule hiltRule;
    
    public RepoListFragmentTest() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.danich.gitlist.ui.repositoriesList.RepositoriesViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    com.danich.gitlist.ui.repositoriesList.RepositoriesViewModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.danich.gitlist.domain.usecases.GetReposUseCase getGetReposUseCase() {
        return null;
    }
    
    public final void setGetReposUseCase(@org.jetbrains.annotations.NotNull()
    com.danich.gitlist.domain.usecases.GetReposUseCase p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.junit.Rule(order = 0)
    public final dagger.hilt.android.testing.HiltAndroidRule getHiltRule() {
        return null;
    }
    
    public final void setHiltRule(@org.jetbrains.annotations.NotNull()
    dagger.hilt.android.testing.HiltAndroidRule p0) {
    }
    
    @org.junit.Before()
    public final void init() {
    }
    
    @org.junit.Test()
    public final void testRepositoriesViewModel() {
    }
    
    @org.junit.Test()
    public final void mainActivityTest() {
    }
    
    @org.junit.Test()
    public final void repositoriesFragmentTest() {
    }
    
    @org.junit.Test()
    public final void testBtnRetry() {
    }
}