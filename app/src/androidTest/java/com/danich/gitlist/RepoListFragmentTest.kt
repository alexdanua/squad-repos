package com.danich.gitlist

import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.danich.gitlist.domain.usecases.GetReposUseCase
import com.danich.gitlist.ui.repositoriesList.RepositoriesFragment
import com.danich.gitlist.ui.repositoriesList.RepositoriesViewModel
import com.danich.gitlist.ui.startActivity.MainActivity
import com.danich.gitlist.utils.launchFragmentInHiltContainer
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject


@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class RepoListFragmentTest {

    lateinit var viewModel: RepositoriesViewModel

    @Inject
    lateinit var getReposUseCase: GetReposUseCase

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun testRepositoriesViewModel() {//TODO need to fix, data not observed
        viewModel = RepositoriesViewModel(getReposUseCase)
        viewModel.getRepos()

        GlobalScope.launch(Dispatchers.Main) {
            viewModel.repoList.observeForever {
                val isNullOrEmpty = it.peekContent().isNullOrEmpty()
                assertThat(isNullOrEmpty).isFalse()
            }
        }
    }

    @Test
    fun mainActivityTest() {
        val scenario = launch(MainActivity::class.java)
    }

    @Test
    fun repositoriesFragmentTest() {
        val scenario = launchFragmentInHiltContainer<RepositoriesFragment>()
    }

    @Test
    fun testBtnRetry() {
        onView(withId(R.id.btnRetry)).perform(click())
    }


}