package com.danich.gitlist

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GitApplication:Application() {
}