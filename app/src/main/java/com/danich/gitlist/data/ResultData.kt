package com.danich.gitlist.data

sealed class ResultData<out T> {
    data class Success<out T>(val data: T) : ResultData<T>()
    data class Error(val errorMsg: String) : ResultData<Nothing>()
}