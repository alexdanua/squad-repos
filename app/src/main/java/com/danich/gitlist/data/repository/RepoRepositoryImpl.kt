package com.danich.gitlist.data.repository

import com.danich.gitlist.data.ResultData
import com.danich.gitlist.domain.models.Repo
import com.danich.gitlist.domain.interfaces.RepoRepository
import com.danich.gitlist.network.ApiService
import com.danich.gitlist.network.mappers.RepoMapper
import javax.inject.Inject

class RepoRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val repoMapper: RepoMapper
) : RepoRepository {

    override suspend fun getRepos(): ResultData<List<Repo>> {
        val response = apiService.getRepos()

        return if (response.isSuccessful && !response.body().isNullOrEmpty()) {
            ResultData.Success(repoMapper.mapFromRemoteList(response.body()!!))
        } else {
            ResultData.Error(response.message())
        }
    }
}