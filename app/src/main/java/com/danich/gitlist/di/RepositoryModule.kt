package com.danich.gitlist.di


import com.danich.gitlist.domain.interfaces.RepoRepository
import com.danich.gitlist.data.repository.RepoRepositoryImpl
import com.danich.gitlist.network.ApiService
import com.danich.gitlist.network.mappers.RepoMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideRepoRepository(
        apiService: ApiService,
        repoMapper: RepoMapper
    ) : RepoRepository = RepoRepositoryImpl(
        apiService,
        repoMapper
    )

}