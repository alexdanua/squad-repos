package com.danich.gitlist.domain.interfaces

import com.danich.gitlist.data.ResultData
import com.danich.gitlist.domain.models.Repo

interface RepoRepository {
     suspend fun getRepos():ResultData<List<Repo>>
}