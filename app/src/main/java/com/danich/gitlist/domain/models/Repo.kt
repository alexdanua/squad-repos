package com.danich.gitlist.domain.models

data class Repo(
    val name: String,
    val description: String
)
