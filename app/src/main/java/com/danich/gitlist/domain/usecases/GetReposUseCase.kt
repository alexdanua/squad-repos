package com.danich.gitlist.domain.usecases

import com.danich.gitlist.data.ResultData
import com.danich.gitlist.domain.models.Repo
import com.danich.gitlist.domain.interfaces.RepoRepository
import javax.inject.Inject

class GetReposUseCase @Inject constructor(
    private val repository: RepoRepository
) {

    suspend operator fun invoke(): ResultData<List<Repo>> {
        return repository.getRepos()
    }
}