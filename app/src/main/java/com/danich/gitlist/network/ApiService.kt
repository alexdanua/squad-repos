package com.danich.gitlist.network

import com.danich.gitlist.network.models.RepoRemote
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("orgs/square/repos")
    suspend fun getRepos() : Response<List<RepoRemote>>
}