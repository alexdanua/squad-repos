package com.danich.gitlist.network.mappers

interface EntityMapper<RemoteModel, DomainModel> {

    fun  mapFromRemote(remoteModel: RemoteModel) : DomainModel

}