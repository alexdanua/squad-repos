package com.danich.gitlist.network.mappers

import com.danich.gitlist.domain.models.Repo
import com.danich.gitlist.network.models.RepoRemote
import javax.inject.Inject


class RepoMapper
@Inject
constructor() : EntityMapper<RepoRemote, Repo> {

    fun mapFromRemoteList(remotePosts: List<RepoRemote>): List<Repo> {
        return remotePosts.map { mapFromRemote(it) }
    }

    override fun mapFromRemote(entity: RepoRemote): Repo {
        return Repo(
            name = entity.name ?: "",
            description = entity.description ?: "",
        )
    }

}






