package com.danich.gitlist.network.models

data class RepoRemote(
    val name: String?,
    val description: String?
)
