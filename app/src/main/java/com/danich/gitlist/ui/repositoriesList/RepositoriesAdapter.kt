package com.danich.gitlist.ui.repositoriesList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.danich.gitlist.R
import com.danich.gitlist.domain.models.Repo
import com.danich.gitlist.databinding.ItemRepoBinding

class RepositoriesAdapter :
    RecyclerView.Adapter<RepositoriesAdapter.RepoViewHolder>() {

    private var repoList = listOf<Repo>()
    fun addItems(items: List<Repo>) {
        this.repoList = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        return RepoViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.item_repo, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        holder.bind(repoList.get(position))
    }

    override fun getItemCount(): Int {
        return repoList.size
    }


    inner class RepoViewHolder(private val binding: ItemRepoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(entity: Repo?) {
            entity?.let { binding.repoEntity = it }
        }
    }


}