package com.danich.gitlist.ui.repositoriesList

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.danich.gitlist.R
import com.danich.gitlist.databinding.FragmentRepositoriesBinding
import com.danich.gitlist.network.isOnline
import com.danich.gitlist.utils.EventObserver
import com.danich.gitlist.utils.showDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RepositoriesFragment : Fragment() {

    private val viewModel by viewModels<RepositoriesViewModel>()

    lateinit var repositoriesAdapter: RepositoriesAdapter

    private var binding: FragmentRepositoriesBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRepositoriesBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //init recyclerView
        binding?.recyclerView?.apply {
            layoutManager = LinearLayoutManager(context)
            repositoriesAdapter = RepositoriesAdapter()
            adapter = repositoriesAdapter
        }
        //init button Retry
        binding?.btnRetry?.setOnClickListener {
            viewModel.getRepos()
        }

        observeRepos()
        observeErrors()
    }

    override fun onStart() {
        super.onStart()
        if (!isInternetAvailable()) return
        viewModel.getRepos()
    }

    private fun observeRepos() {
        viewModel.repoList.observe(viewLifecycleOwner, EventObserver {
            binding?.progressBar?.visibility = View.GONE
            repositoriesAdapter.addItems(it)
        })
    }

    private fun observeErrors() {
        viewModel.errorText.observe(viewLifecycleOwner, EventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun isInternetAvailable(): Boolean {
        if (!isOnline(requireContext())) {
            binding?.btnRetry?.visibility = View.VISIBLE
            binding?.tvInfo?.visibility = View.VISIBLE
            binding?.progressBar?.visibility = View.GONE
            showDialog()
            return false
        } else {
            binding?.btnRetry?.visibility = View.GONE
            binding?.tvInfo?.visibility = View.GONE
            binding?.progressBar?.visibility = View.VISIBLE
            return true
        }
    }

    private fun showDialog() {
        requireContext().showDialog(
            title = getString(R.string.error_title_no_internet),
            message = getString(R.string.error_msg_no_internet)
        ) {
            startActivity(Intent(Settings.ACTION_WIRELESS_SETTINGS))
        }
    }


}