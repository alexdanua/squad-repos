package com.danich.gitlist.ui.repositoriesList

import androidx.lifecycle.*
import com.danich.gitlist.data.ResultData
import com.danich.gitlist.domain.usecases.GetReposUseCase
import com.danich.gitlist.domain.models.Repo
import com.danich.gitlist.utils.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RepositoriesViewModel @Inject constructor(
    private val getReposUseCase: GetReposUseCase
): ViewModel() {

    private val _errorText = MutableLiveData<Event<String>>()
    val errorText: LiveData<Event<String>> = _errorText

    private val _repoList = MutableLiveData<Event<List<Repo>>>()
    val repoList: LiveData<Event<List<Repo>>> = _repoList


    fun getRepos(){
       viewModelScope.launch {
           getReposUseCase.invoke().also {
               if (it is ResultData.Success){
                   _repoList.value =  Event(it.data)
               }else if (it is ResultData.Error){
                   _errorText.value = Event(it.errorMsg)
               }
           }
       }
    }

}