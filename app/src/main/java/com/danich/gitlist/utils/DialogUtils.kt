package com.danich.gitlist.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog

 fun Context.showDialog(title: String, message: String, positiveAction: () -> Unit) {
    AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
        .setPositiveButton(android.R.string.ok) { dialog, _ ->
            dialog.dismiss()
            positiveAction()
        }
        .show()
}